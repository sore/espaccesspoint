#include <WiFi.h>
#include <EEPROM.h>

#define EEPROM_size 200

// _____________Properties_____________

WiFiServer server(80);
String header;

IPAddress AP_IP = IPAddress (192, 168, 4, 2);
IPAddress AP_gateway = IPAddress (10, 10, 2, 8);
IPAddress AP_NMask = IPAddress (255, 255, 255, 0);

IPAddress STA_IP = IPAddress (192, 168, 15, 127);
IPAddress STA_GETEWAY = IPAddress (10, 10, 2, 8);
IPAddress subnet = IPAddress (255, 255, 0, 0);
IPAddress primaryDNS = IPAddress (8, 8, 8, 8); 
IPAddress secondaryDNS = IPAddress (8, 8, 4, 4);

const char* selectedConnectionSSIDKey = "selectedConnectionSSID";
const char* selectedConnectionPasswordKey = "selectedConnectionPassword";
const char* accessPointSSID = "AccessPoint";
const char* accessPointPassword = "accesspoint123";

const int selectedConnectionSSIDPosition = 0;
const int selectedConnectionPasswordPosition = 100;

unsigned long currentTime = millis();
unsigned long previousTime = 0;
const long timeoutTime = 2000;

// _____________Methods_____________

void setup() {
  Serial.begin(115200);
  startStorage();
  String selectedConnectionSSID = read(selectedConnectionSSIDPosition);
  String selectedConnectionPassword = read(selectedConnectionPasswordPosition);
  customPrint("selectedConnectionSSID: ");
  customPrintln(selectedConnectionSSID);
  customPrint("selectedConnectionPassword: ");
  customPrintln(selectedConnectionPassword);
  if (selectedConnectionSSID.length() == 0 || selectedConnectionPassword.length() == 0) {
    WiFi.mode(WIFI_AP);  
    WiFi.softAP(accessPointSSID, accessPointPassword);
    delay(1000);
    WiFi.softAPConfig(AP_IP, AP_IP, AP_NMask);
    delay(1000);
    String myIP = WiFi.softAPIP().toString();
    customPrintln("");
    customPrint("AP IP address: ");
    customPrintln(myIP);
  } else {
    //WiFi.mode(WIFI_STA);
    if(!WiFi.config(STA_IP, STA_GETEWAY, subnet, primaryDNS, secondaryDNS)) {
      customPrintln("STA Failed to configure");
    }
    WiFi.begin(&selectedConnectionSSID[0], &selectedConnectionPassword[0]);
    int disconnectedLoops = 0;
    while (WiFi.status() != WL_CONNECTED) {
      if (disconnectedLoops > 40) {
        clearStorage();
        break;
      } else {
        delay(500);
        customPrint(".");
        disconnectedLoops++;
      }
    }
    String myIP = WiFi.localIP().toString();
    customPrint("IP address: ");
    customPrintln(myIP);
  }
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    currentTime = millis();
    previousTime = currentTime;
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected() && currentTime - previousTime <= timeoutTime) {  // loop while the client's connected
      currentTime = millis();
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            if (header.indexOf("GET /save") >= 0) {
              customPrintln("save");
              client.print("Salvar");
            } else if (header.indexOf("GET /retrieve") >= 0) {
              client.print("Recuperar");
              customPrintln("retrieve");
            } else if (header.indexOf("GET /config") >= 0) {
              customPrintln("config");
              int ssidIndex = header.indexOf("ssid=");
              int passwordIndex = header.indexOf("password=");
              int eIndex = header.indexOf("&&");
              if (ssidIndex >= 0 && passwordIndex >= 0 && eIndex >= 0) {
                int ssidLastIndex = passwordIndex - 1;
                int passwordLastIndex = header.length() - 1;
                String ssid = header.substring(ssidIndex + 5, ssidLastIndex);
                String password = header.substring(passwordIndex + 9, eIndex);
                customPrint("ssid: ");
                customPrintln(ssid);
                customPrint("password: ");
                customPrintln(password);
                save(selectedConnectionSSIDPosition, ssid);
                save(selectedConnectionPasswordPosition, password);
                client.print("Success to save ");
                client.print(ssid);
                client.print(" WiFi.");
              } else {
                client.print("Fail to get ssid or password");
              }
            } else if (header.indexOf("GET /reset") >= 0) {
              customPrintln("reset");
              clearStorage();
              client.print("Success to reset!");
            }
            
            client.println();
            break;
          } else {
            currentLine = "";
          }
        } else if (c != '\r') {
          currentLine += c;
        }
      }
    }
    header = "";
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

// _____________Storage Methods_____________

void startStorage() {
  EEPROM.begin(EEPROM_size);
}

void save(int address, String value) {
  byte len = value.length();
  EEPROM.write(address, len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(address + 1 + i, value[i]);
  }
  EEPROM.commit();
}

String read(int address) {
  byte len = EEPROM.read(address);
  if (len == 0) {
    return "";
  }
  char data[len + 1];
  for (int i = 0; i < len; i++) {
    data[i] = EEPROM.read(address + 1 + i);
  }
  data[len] = '\0';
  return String(data);
}

void remove(int address) {

}

void clearStorage() {
  for (int i = 0; i < EEPROM_size; i++) {
    EEPROM.write(i, 0);
  }
  EEPROM.commit();
}

// _____________Print Methods_____________

void customPrint(String text) {
  Serial.print(text);
}

void customPrintln(String text) {
  Serial.println(text);
}
